//
//  Collection.swift
//  ProductHunt
//
//  Created by Cédric Rolland on 01/12/15.
//  Copyright © 2015 Cédric Rolland. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class Product: NSObject, NSCoding {
    var id: Int
    var name: String
    var title: String
    var createdAt: String
    var updatedAt: String
    var color: String
    var subscriberCount: Int
    var collectionUrl: String
    var postsCount: Int
    var user: User
    var productsArray = NSMutableArray()
    
    // MARK: - Initializers
    
    required init(productsArray: NSMutableArray, id: Int, name: String, title: String, createdAt: String, updatedAt: String, color: String, subscriberCount: Int, collectionUrl: String, postsCount: Int, user: User) {
        self.productsArray = productsArray
        self.id = id
        self.name = name
        self.title = title
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.color = color
        self.subscriberCount = subscriberCount
        self.collectionUrl = collectionUrl
        self.postsCount = postsCount
        self.user = user
    }
    
    required convenience init?(json: JSON) {
        self.init(
            productsArray: NSMutableArray(),
            id: json[ProductFields.Id.rawValue].intValue,
            name: json[ProductFields.Name.rawValue].stringValue,
            title: json[ProductFields.Title.rawValue].stringValue,
            createdAt: json[ProductFields.CreatedAt.rawValue].stringValue,
            updatedAt: json[ProductFields.UpdatedAt.rawValue].stringValue,
            color: json[ProductFields.Color.rawValue].stringValue,
            subscriberCount: json[ProductFields.SubscriberCount.rawValue].intValue,
            collectionUrl: json[ProductFields.CollectionUrl.rawValue].stringValue,
            postsCount: json[ProductFields.PostsCount.rawValue].intValue,
            user: User(json: json[ProductFields.User.rawValue])!
        )
    }
    
    // MARK: - Network
    
    class func getProducts(completionHandler: (AnyObject?, NSError?) -> ()) {
        let headers = [
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": "Bearer 3629dffd0e6ee8cd66089261e33f16e7746a333fecba78570c99f553e2e2af6d",
            "Host": "api.producthunt.com"
        ]
        
        Alamofire.request(.GET, "https://api.producthunt.com/v1/collections", encoding: .JSON, headers: headers)
            .responseJSON { response in
                completionHandler(response.result.value, response.result.error)
        }
    }
    
    // MARK: - NSKeyedArchiver
    
    required convenience init?(coder aDecoder: NSCoder) {
        let productsArray = aDecoder.decodeObjectForKey("productsArray") as! NSMutableArray
        let id = aDecoder.decodeIntegerForKey(ProductFields.Id.rawValue)
        let name = aDecoder.decodeObjectForKey(ProductFields.Name.rawValue) as! String
        let title = aDecoder.decodeObjectForKey(ProductFields.Title.rawValue) as! String
        let createdAt = aDecoder.decodeObjectForKey(ProductFields.CreatedAt.rawValue) as! String
        let updatedAt = aDecoder.decodeObjectForKey(ProductFields.UpdatedAt.rawValue) as! String
        let color = aDecoder.decodeObjectForKey(ProductFields.Color.rawValue) as! String
        let subscriberCount = aDecoder.decodeIntegerForKey(ProductFields.SubscriberCount.rawValue)
        let collectionUrl = aDecoder.decodeObjectForKey(ProductFields.CollectionUrl.rawValue) as! String
        let postsCount = aDecoder.decodeIntegerForKey(ProductFields.PostsCount.rawValue)
        let user = aDecoder.decodeObjectForKey(ProductFields.User.rawValue) as! User
        
        self.init(productsArray: productsArray, id: id, name: name, title: title, createdAt: createdAt, updatedAt: updatedAt, color: color, subscriberCount: subscriberCount, collectionUrl: collectionUrl, postsCount: postsCount, user: user)
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(productsArray, forKey: "productsArray")
        aCoder.encodeInteger(id, forKey: ProductFields.Id.rawValue)
        aCoder.encodeObject(name, forKey: ProductFields.Name.rawValue)
        aCoder.encodeObject(title, forKey: ProductFields.Title.rawValue)
        aCoder.encodeObject(createdAt, forKey: ProductFields.CreatedAt.rawValue)
        aCoder.encodeObject(updatedAt, forKey: ProductFields.UpdatedAt.rawValue)
        aCoder.encodeObject(color, forKey: ProductFields.Color.rawValue)
        aCoder.encodeInteger(subscriberCount, forKey: ProductFields.SubscriberCount.rawValue)
        aCoder.encodeObject(collectionUrl, forKey: ProductFields.CollectionUrl.rawValue)
        aCoder.encodeInteger(postsCount, forKey: ProductFields.PostsCount.rawValue)
        aCoder.encodeObject(user, forKey: ProductFields.User.rawValue)
    }
    
    func save() {
        let data = NSKeyedArchiver.archivedDataWithRootObject(self)
        NSUserDefaults.standardUserDefaults().setObject(data, forKey: "collectionKey")
    }
    
    func clear() {
        NSUserDefaults.standardUserDefaults().removeObjectForKey("collectionKey")
    }
    
    class func loadSaved() -> Product? {
        if let data = NSUserDefaults.standardUserDefaults().objectForKey("collectionKey") as? NSData {
            return NSKeyedUnarchiver.unarchiveObjectWithData(data) as? Product
        }
        return nil
    }
    
    class func saveProduct(productsArrayToSave: Array<Product>) {
        let productsLoaded = loadSaved()
        
        for x in productsArrayToSave {
            productsLoaded?.productsArray.addObject(x)
        }
        
        productsLoaded?.save()
    }

    class func loadProductsArray() -> Array<Product> {
        let productLoaded = loadSaved()
        
        var productsArrayToReturn: Array<Product> = []
        
        if productLoaded?.productsArray != nil {
            for product in productLoaded!.productsArray {
                productsArrayToReturn.append(product as! Product)
            }
        }
        
        print("\(productsArrayToReturn.count) products loaded from memory.")
        
        return productsArrayToReturn
    }
    
    class func saveProductsToDisk() {

        let path = fileInDocumentsDirectory("product")
        var successProduct = false
        
        if let product = loadSaved() {
            successProduct = NSKeyedArchiver.archiveRootObject(product.productsArray, toFile: path)
        }

        successProduct ? print("products saved successfully") : print("Error saving product file")
    }
    
    class func loadProductsFromDisk(){

        let productsLoaded = loadSaved()
        let path = fileInDocumentsDirectory("product")
        var productArray = NSMutableArray()
        
        if let myProduct = NSKeyedUnarchiver.unarchiveObjectWithFile(path) as! NSMutableArray? {
            productArray = myProduct
        }
        
        productsLoaded?.productsArray = productArray
        productsLoaded?.save()
    }
    
    // The following two functions are used to generate the path where to save/load the data to/from disk.
    class func documentsDirectory() -> String {
        let documentsFolderPath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0]
        return documentsFolderPath
    }
    
    /// Generates the path where to save/load the data to/from disk.
    ///
    /// - parameter The: name of the file that will contain the data
    /// - returns: The path where to save the file.
    class func fileInDocumentsDirectory(filename: String) -> String {
        return documentsDirectory().stringByAppendingPathComponent(filename)
    }
}

enum ProductFields: String {
    case Id = "id"
    case Name = "name"
    case Title = "title"
    case CreatedAt = "created_at"
    case UpdatedAt = "updated_at"
    case Color = "color"
    case SubscriberCount = "subscriber_count"
    case CollectionUrl = "collection_url"
    case PostsCount = "posts_count"
    case User = "user"
}