//
//  Authentication.swift
//  ProductHunt
//
//  Created by Cédric Rolland on 29/11/15.
//  Copyright © 2015 Cédric Rolland. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class Authentication: NSObject, NSCoding {
    
    var accessToken: String
    
    // MARK: - Initializers
    
    required init(accessToken: String) {
        self.accessToken = accessToken
    }
    
    required convenience init?(json: JSON) {
        self.init(
            accessToken: json[AuthFields.AccessToken.rawValue].stringValue
        )
    }
    
    // MARK: - Networking
    
    class func getData(completionHandler: (AnyObject?, NSError?) -> ()) {
        
        let params = [
            "client_id" : "350b2a3578c634c97b33b55bc3cfdea8adddd02e70b1f974aa5400c8c5399c46",
            "client_secret" : "e416b796d4aca59e5b496dffb0841fbfa91fdfdb6f873e49c088fca48db207ea",
            "grant_type" : "client_credentials"
        ]
        
        let headers = [
            "Content-Type" : "application/json",
            "Accept" : "application/json",
            "Host" :"api.producthunt.com"
        ]
        
        Alamofire.request(.POST, "https://api.producthunt.com/v1/oauth/token", parameters: params, encoding: .JSON, headers: headers)
            .responseJSON { response in
                completionHandler(response.result.value, response.result.error)
        }
    }
    
    // MARK: - NSKeyedArchiver
    
    required convenience init?(coder aDecoder: NSCoder) {
        let accessToken = aDecoder.decodeObjectForKey(AuthFields.AccessToken.rawValue) as! String
        self.init(accessToken: accessToken)
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(accessToken, forKey: AuthFields.AccessToken.rawValue)
    }
    
    func save() {
        let data = NSKeyedArchiver.archivedDataWithRootObject(self)
        NSUserDefaults.standardUserDefaults().setObject(data, forKey: "accessToken")
    }
    
    class func loadSaved() -> Authentication? {
        if let data = NSUserDefaults.standardUserDefaults().objectForKey("accessToken") as? NSData {
            return NSKeyedUnarchiver.unarchiveObjectWithData(data) as? Authentication
        }
        return nil
    }
    
}

enum AuthFields: String {
    case AccessToken = "access_token"
}


