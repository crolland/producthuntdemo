//
//  Extensions.swift
//  ProductHunt
//
//  Created by Cédric Rolland on 05/12/15.
//  Copyright © 2015 Cédric Rolland. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(rgb: UInt) {
        self.init(
            red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgb & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

extension String {
    func stringByAppendingPathComponent(path: String) -> String {
        let nsSt = self as NSString
        return nsSt.stringByAppendingPathComponent(path)
    }
    
    func substringToIndex(to: Int) -> String {
        let nsSt = self as NSString
        return nsSt.substringToIndex(to)
    }
    
    func formatDate(dateInputFormat: String) -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = dateInputFormat
        let date = dateFormatter.dateFromString(self)
        
        let formatter = NSDateFormatter()
        formatter.dateStyle = .FullStyle
        
        return formatter.stringFromDate(date!)
    }
}