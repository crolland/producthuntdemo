//
//  ProductCell.swift
//  ProductHunt
//
//  Created by Cédric Rolland on 03/12/15.
//  Copyright © 2015 Cédric Rolland. All rights reserved.
//

import UIKit


class ProductCell: UITableViewCell {
    
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productDetails: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var upvoteButton: UIButton!
    
    
    func configure(product: Product, image: UIImage?) {
        productTitle.text = product.name
        productTitle.textColor = UIColor(rgb: 0x494e51)
        
        // This is used to have a well aligned text on multiple lines
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .Left
        
        let attributedDescriptionString = NSAttributedString(string: product.user.headline,
            attributes: [
                NSParagraphStyleAttributeName: paragraphStyle,
                NSBaselineOffsetAttributeName: NSNumber(float: 0)
            ])
        
        productDetails.attributedText = attributedDescriptionString
        productDetails.lineBreakMode = NSLineBreakMode.ByWordWrapping
        productDetails.numberOfLines = 2
        productDetails.textColor = UIColor(rgb: 0x8a8e91)

        upvoteButton.backgroundColor = UIColor(rgb: 0xeaeeef)
        upvoteButton.setTitleColor(UIColor(rgb: 0x494e51), forState: .Normal)
        upvoteButton.layer.cornerRadius = 5
        upvoteButton.layer.borderWidth = 0
        upvoteButton.setTitle(String(product.postsCount), forState: .Normal)
        
        if (image != nil) {
            icon.image = image
        }
        icon.layer.masksToBounds = true
        icon.layer.cornerRadius = 25
        icon.layer.borderWidth = 0

    }
}
