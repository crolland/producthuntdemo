//
//  ProductsTableViewController.swift
//  ProductHunt
//
//  Created by Cédric Rolland on 28/11/15.
//  Copyright © 2015 Cédric Rolland. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProductsTableViewController: UITableViewController {

    var productsArray: Array<Product> = []
    var dateKeys: Array<String> = []
    var collection: Array<Array<Product>> = []
    var images: Array<UIImage> = []
    
    @IBOutlet weak var productsTableView: UITableView?
    
    // MARK: - View
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.productsArray = Product.loadProductsArray()
        
        if (self.productsArray.count == 0) {
            self.loadCollections()
        } else {
            self.processData(self.productsArray)
            self.createCollection()
            self.productsTableView?.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - custom functions
    
    func loadCollections() {
        
        print("No data on disk")
        print("Starting fetching data")

        Product.getProducts() { responseObject, error in
            var productsToProcess: Array<Product> = []
            let products = SwiftyJSON.JSON(responseObject!)["collections"]
                        
            for product in products {
                productsToProcess.append(Product(json: product.1)!)
            }
            self.processData(productsToProcess)
            print("Finish fetching data: \(self.productsArray.count) elements retrieved")
        }
    }
    
    func processData(dataToProcess: Array<Product>) {
        let keySet = NSMutableSet()
        
        for productToAdd in dataToProcess {
            let croppedDate = productToAdd.updatedAt.substringToIndex(10)
            
            keySet.addObject(croppedDate)
            productToAdd.updatedAt = croppedDate
            
            if (self.productsArray.count < 50) {
                self.productsArray.append(productToAdd)
            }
            
            self.getUserImages(productToAdd.user.imagesUrl["88px@3X"]!)
        }
        
        print("products to save: \(self.productsArray.count)")
        Product.saveProduct(self.productsArray)
        
        let sort = [NSSortDescriptor(key: "description", ascending: false)]
        self.dateKeys = keySet.sortedArrayUsingDescriptors(sort) as! [String]
        
        self.createCollection()
        self.productsTableView?.reloadData()
        
    }
    
    func createCollection() {
        for _ in dateKeys {
            collection.append(Array<Product>())
        }
        
        for product in productsArray {
            let i = dateKeys.indexOf(product.updatedAt)
            collection[i!].append(product)
        }
    }
    
    func getJSON(urlToRequest: String) -> NSData{
        return NSData(contentsOfURL: NSURL(string: urlToRequest)!)!
    }
    
    func getUserImages(url: String){
        Alamofire.request(.GET, url).response() {
            (_, _, data, _) in
            self.images.append(UIImage(data: data!)!)
            self.productsTableView?.reloadData()
        }
    }

    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UILabel(frame: CGRectMake(0, 0, tableView.frame.size.width, 40))
        
        header.text = dateKeys[section].formatDate("yyyy-MM-dd")
        header.backgroundColor = UIColor(rgb: 0xf7f9f8)
        header.textAlignment = .Center
        header.font = UIFont(name: "AvenirNext-Bold", size: 14)
        header.textColor = UIColor.grayColor()
        
        return header
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return dateKeys.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let products = self.collection[section]
        return products.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ProductCell", forIndexPath: indexPath) as! ProductCell
        
        let products = collection[indexPath.section]
        let product = products[indexPath.row]
        
        if (self.images.count < products.count) {
            cell.configure(product, image: nil)
        } else {
            cell.configure(product, image: self.images[indexPath.row])
        }
        
        return cell
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
