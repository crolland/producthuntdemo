//
//  User.swift
//  ProductHunt
//
//  Created by Cédric Rolland on 01/12/15.
//  Copyright © 2015 Cédric Rolland. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class User: NSObject, NSCoding {
    var id: Int
    var name: String
    var headline: String
    var createdAt: String
    var username: String
    var websiteUrl: String
    var profileUrl: String
    var imagesUrl: [String : String]
    
    // MARK: - Initializers

    required init(id: Int, name: String, headline: String, createdAt: String, username: String, websiteUrl: String, profileUrl: String, imagesUrl: [String : String]) {
        self.id = id
        self.name = name
        self.headline = headline
        self.createdAt = createdAt
        self.username = username
        self.websiteUrl = websiteUrl
        self.profileUrl = profileUrl
        self.imagesUrl = imagesUrl
    }
    
    required convenience init?(json: JSON) {
        self.init(
            id: json[UserFields.Id.rawValue].intValue,
            name: json[UserFields.Name.rawValue].stringValue,
            headline: json[UserFields.Headline.rawValue].stringValue,
            createdAt: json[UserFields.CreatedAt.rawValue].stringValue,
            username: json[UserFields.Username.rawValue].stringValue,
            websiteUrl: json[UserFields.WebsiteUrl.rawValue].stringValue,
            profileUrl: json[UserFields.ProfileUrl.rawValue].stringValue,
            imagesUrl: json[UserFields.ImagesUrl.rawValue].dictionaryObject as! [String : String]
        )
    }
    
    // MARK: - NSKeyedArchiver
    
    required convenience init?(coder aDecoder: NSCoder) {
        let id = aDecoder.decodeIntegerForKey(UserFields.Id.rawValue)
        let name = aDecoder.decodeObjectForKey(UserFields.Name.rawValue) as! String
        let headline = aDecoder.decodeObjectForKey(UserFields.Headline.rawValue) as! String
        let createdAt = aDecoder.decodeObjectForKey(UserFields.CreatedAt.rawValue) as! String
        let username = aDecoder.decodeObjectForKey(UserFields.Username.rawValue) as! String
        let websiteUrl = aDecoder.decodeObjectForKey(UserFields.WebsiteUrl.rawValue) as! String
        let profileUrl = aDecoder.decodeObjectForKey(UserFields.ProfileUrl.rawValue) as! String
        let imagesUrl = aDecoder.decodeObjectForKey(UserFields.ImagesUrl.rawValue) as! [String : String]
        
        self.init(id: id, name: name, headline: headline, createdAt: createdAt, username: username, websiteUrl: websiteUrl, profileUrl: profileUrl, imagesUrl: imagesUrl)
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeInteger(id, forKey: UserFields.Id.rawValue)
        aCoder.encodeObject(name, forKey: UserFields.Name.rawValue)
        aCoder.encodeObject(headline, forKey: UserFields.Headline.rawValue)
        aCoder.encodeObject(createdAt, forKey: UserFields.CreatedAt.rawValue)
        aCoder.encodeObject(username, forKey: UserFields.Username.rawValue)
        aCoder.encodeObject(websiteUrl, forKey: UserFields.WebsiteUrl.rawValue)
        aCoder.encodeObject(profileUrl, forKey: UserFields.ProfileUrl.rawValue)
        aCoder.encodeObject(imagesUrl, forKey: UserFields.ImagesUrl.rawValue)
    }
}

enum UserFields: String {
    case Id = "id"
    case Name = "name"
    case Headline = "headline"
    case CreatedAt = "created_at"
    case Username = "username"
    case WebsiteUrl = "website_url"
    case ProfileUrl = "profile_url"
    case ImagesUrl = "image_url"
}